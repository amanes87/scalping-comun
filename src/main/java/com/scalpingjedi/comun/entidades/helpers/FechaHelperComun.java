package com.scalpingjedi.comun.entidades.helpers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "singleton")
public class FechaHelperComun {

	public static Long getTimeDeLocalDateTime(LocalDateTime fecha) {
		return Date.from(fecha.atZone(ZoneOffset.UTC).toInstant()).getTime();
	}
	
	public static LocalDateTime dateToLocalDateTime(Date dateToConvert) {
	    return dateToConvert.toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
	}
}
