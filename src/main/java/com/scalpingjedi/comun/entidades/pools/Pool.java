package com.scalpingjedi.comun.entidades.pools;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "POOLS")
public class Pool {

	@Id
	private int id;
	
	private LocalDateTime hora;

	private Integer precio;
	
	private BigDecimal volumen;
	
	private String apalancamiento;
	
	private int id2;

	public Pool(LocalDateTime hora, Integer precio, BigDecimal volumen, String apalancamiento, int id2) {
		this.hora = hora;
		this.precio = precio;
		this.volumen = volumen;
		this.apalancamiento = apalancamiento;
		id = this.hashCode();
		this.id2 = id2;
	}
	
	

}
