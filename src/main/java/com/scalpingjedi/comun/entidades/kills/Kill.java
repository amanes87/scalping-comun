package com.scalpingjedi.comun.entidades.kills;

import java.time.Instant;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.scalpingjedi.comun.entidades.helpers.FechaHelperComun;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "KILL")
public class Kill {

	@Id
	private int id;

	private LocalDateTime fecha;

	private String killType;

	private Double diff;

	private String side;

	@Transient
	private Long fechaLong;

	public Kill(LocalDateTime fecha, double diff, String side) {
		this.fecha = fecha;
		String killType = "";
		if (diff > 0.25 && diff < 1) {
			killType = "Kill";
		} else if (diff >= 1 && diff < 2.3) {
			killType = "Fury Kill";
		} else if (diff >= 2.3) {
			killType = "Blood Kill";
		}
		this.killType = killType;
		this.diff = diff;
		this.side = side;
		this.fechaLong = FechaHelperComun.getTimeDeLocalDateTime(fecha);
		id = this.hashCode();
	}
}
