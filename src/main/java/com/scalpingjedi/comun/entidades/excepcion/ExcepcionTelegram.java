package com.scalpingjedi.comun.entidades.excepcion;

import java.text.MessageFormat;

import com.telegram.service.TelegramSv;

public class ExcepcionTelegram extends Exception{

	private static final long serialVersionUID = 2324673316252712906L;
	
	private String mensaje;
	
	public ExcepcionTelegram(Throwable e, String mensaje) {
		super(e);
		this.mensaje = mensaje;
	}
	
	public ExcepcionTelegram(Throwable e, String mensaje, TelegramSv telegramSv) {
		super(e);
		this.mensaje = mensaje;
		if (telegramSv != null) {
			telegramSv.notifyMessage(MessageFormat.format("{0} | {1}", this.mensaje, e.getMessage()));
		}
	}
}
