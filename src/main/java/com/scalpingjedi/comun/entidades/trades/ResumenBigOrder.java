package com.scalpingjedi.comun.entidades.trades;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.scalpingjedi.comun.entidades.dto.trade.TradeData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "RESUMEN_BIG_ORDERS")
public class ResumenBigOrder {

	@Id
	private LocalDateTime fecha;

	private Integer cant1000;
	
	private Integer cant5000;
	
	private Integer cant20000;

	private Integer cant100000;
	
	private Integer cant250000;
	
	private Integer cant500000;
	
	private Integer cantINF;
	
	public ResumenBigOrder(TradeData tradeData) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		this.fecha = LocalDateTime.parse(tradeData.getTimestamp(), formatter).plusMinutes(5);
		this.cant1000 = 0;
		this.cant5000 = 0; 
		this.cant20000 = 0;
		this.cant100000 = 0;
		this.cant250000 = 0;
		this.cant500000 = 0;
		this.cantINF = 0;
		acumularDatos(tradeData);
	}
	
	public void acumularDatos(TradeData tradeData) {
		if (tradeData.getSize().compareTo(new BigDecimal(500000)) > 0) { //INF
			this.cantINF++;
		} else if (tradeData.getSize().compareTo(new BigDecimal(250000)) > 0) { //500.000
			this.cant500000++;
		}else if (tradeData.getSize().compareTo(new BigDecimal(100000)) > 0) { //250.000
			this.cant250000++;
		}else if (tradeData.getSize().compareTo(new BigDecimal(20000)) > 0) {//100.000
			this.cant100000++;
		}else if (tradeData.getSize().compareTo(new BigDecimal(5000)) > 0) {//20.000
			this.cant20000++;
		}else if (tradeData.getSize().compareTo(new BigDecimal(1000)) > 0) {//5.000
			this.cant5000++;
		}else {//1.000
			this.cant1000++;
		}
	}

	public String getFechaSeconds() {
		return "" + this.getFecha().toEpochSecond(ZoneOffset.UTC);
	}
}
