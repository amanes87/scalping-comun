package com.scalpingjedi.comun.entidades.trades;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.scalpingjedi.comun.entidades.dto.trade.TradeData;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "BIG_ORDERS")
public class BigOrder {

	@Id
	private LocalDateTime hora;

	private BigDecimal price;

	private BigDecimal volumen;

	private String side;


	public BigOrder(TradeData trade) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		this.hora = LocalDateTime.parse(trade.getTimestamp(), formatter);
		this.price = new BigDecimal(trade.getPrice());
		this.volumen = trade.getSize();
		this.side = trade.getSide();
	}

	public String getHoraCompletaString() {
		return hora.toString();
	}

}
