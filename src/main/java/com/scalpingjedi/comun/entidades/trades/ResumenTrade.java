package com.scalpingjedi.comun.entidades.trades;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "RESUMEN_TRADE")
public class ResumenTrade {

	@Id
	private LocalDateTime fechaCierre;

	private BigDecimal open;

	private BigDecimal high;

	private BigDecimal low;

	private BigDecimal close;

	// LONGS
	private BigDecimal precioLong;

	private BigDecimal volumenLong;

	private BigDecimal tamanoLong;

	private Integer countLong;

	private BigDecimal volumenMayorLong;

	// SHORT
	private BigDecimal precioShort;

	private BigDecimal volumenShort;

	private BigDecimal tamanoShort;

	private BigDecimal volumenMayorShort;

	private Integer countShort;

	private BigDecimal precioLiquidacion25Long;

	private BigDecimal precioLiquidacion50Long;

	private BigDecimal precioLiquidacion100Long;
	
	private BigDecimal precioLiquidacion25Short;

	private BigDecimal precioLiquidacion50Short;

	private BigDecimal precioLiquidacion100Short;

	public String getFechaCierreString(String patron) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patron);
		String formatDateTime = this.getFechaCierre().atOffset(ZoneOffset.UTC).format(formatter);
		return formatDateTime;
	}

}
