package com.scalpingjedi.comun.entidades;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Locale;
import java.util.function.Predicate;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.scalpingjedi.comun.entidades.dto.tradebin.TradeBinData;
import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;

import io.swagger.client.model.TradeBin;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
public class VelaBitmex {

	@Id
	private LocalDateTime horaCierre;
	private BigDecimal open;
	private BigDecimal close;
	private BigDecimal high;
	private BigDecimal low;
	private BigDecimal precioTmp;
	private BigDecimal volumen;
	private Boolean shortLiquidation;
	private BigDecimal shortLiquidationPrice;
	private Boolean longLiquidation;
	private BigDecimal longLiquidationPrice;
	private Long shortLiquidationQuantity;
	private Long longLiquidationQuantity;
	private Integer cantidadBuy;
	private Integer cantidadSell;

	private BigDecimal precioMedioBuy;
	private BigDecimal volumenBuy;
	private BigDecimal dineroAcumuladoBuy;

	private BigDecimal precioMedioSell;
	private BigDecimal volumenSell;
	private BigDecimal dineroAcumuladoSell;

	public VelaBitmex(DataBitmex data) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		this.horaCierre = LocalDateTime.parse(data.getTimestamp(), formatter);
		this.precioTmp = new BigDecimal(data.getPrice());
	}

	public void setNuevosValores(VelaBitmex vela) {
		if (this.open == null) {
			this.open = vela.getPrecioTmp();
			this.high = vela.getPrecioTmp();
			this.low = vela.getPrecioTmp();
		}
		if (this.high.compareTo(vela.getPrecioTmp()) < 0) {
			this.high = vela.getPrecioTmp();
		}
		if (this.low.compareTo(vela.getPrecioTmp()) > 0) {
			this.low = vela.getPrecioTmp();
		}
		this.close = vela.getPrecioTmp();
	}

	@Override
	public String toString() {
		return "VelaBitmex [horaCierre=" + horaCierre + ", open=" + open + ", close=" + close + ", high=" + high
				+ ", low=" + low + "]";
	}

	public VelaBitmex(TradeBinData tradeBin) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		this.horaCierre = LocalDateTime.parse(tradeBin.getTimestamp(), formatter);
		this.volumen = tradeBin.getVolume();
		this.open = new BigDecimal(tradeBin.getOpen());
		this.high = new BigDecimal(tradeBin.getHigh());
		this.low = new BigDecimal(tradeBin.getLow());
		this.close = new BigDecimal(tradeBin.getClose());
	}

	public VelaBitmex(TradeBinData tradeBin, Liquidacion liquidacion) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		this.horaCierre = LocalDateTime.parse(tradeBin.getTimestamp(), formatter);
		this.volumen = tradeBin.getVolume();
		this.open = new BigDecimal(tradeBin.getOpen());
		this.high = new BigDecimal(tradeBin.getHigh());
		this.low = new BigDecimal(tradeBin.getLow());
		this.close = new BigDecimal(tradeBin.getClose());
		this.longLiquidation = liquidacion.getLongLiquidation();
		this.longLiquidationQuantity = liquidacion.getLongLiquidationQuantity();
		this.shortLiquidation = liquidacion.getShortLiquidation();
		this.shortLiquidationQuantity = liquidacion.getShortLiquidationQuantity();
		this.longLiquidationPrice = liquidacion.getPrecioMedioLongLiquidation();
		this.shortLiquidationPrice = liquidacion.getPrecioMedioShortLiquidation();
	}

	public VelaBitmex(Vela_005 vela) {
		this.horaCierre = vela.getHoraCierre();
		this.open = vela.getOpen();
		this.close = vela.getClose();
		this.high = vela.getHigh();
		this.low = vela.getLow();
		this.precioTmp = vela.getPrecioTmp();
		this.volumen = vela.getVolumen();
	}

	public void cerrarVela(VelaBitmex vela) {
		if (vela.getHigh().compareTo(getHigh()) > 0) {
			setHigh(vela.getHigh());
		}
		if (getLow().compareTo(vela.getLow()) > 0) {
			setLow(vela.getLow());
		}
		setClose(vela.getClose());
		setVolumen(getVolumen().add(vela.getVolumen()));
		setPrecioTmp(vela.getPrecioTmp());
	}

	public VelaBitmex(TradeBin tradeBin) {
		this.horaCierre = LocalDateTime.ofEpochSecond(tradeBin.getTimestamp().toEpochSecond(), 0, ZoneOffset.UTC);
		this.volumen = tradeBin.getVolume();
		this.open = new BigDecimal(tradeBin.getOpen());
		this.high = new BigDecimal(tradeBin.getHigh());
		this.low = new BigDecimal(tradeBin.getLow());
		this.close = new BigDecimal(tradeBin.getClose());
	}

	public VelaBitmex(VelaBitmex vela) {
		this.horaCierre = vela.horaCierre;
		this.volumen = vela.getVolumen();
		this.open = vela.getOpen();
		this.high = vela.getHigh();
		this.low = vela.getLow();
		this.close = vela.getClose();
	}

	public String getHoraCierreString(String patron) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patron);
		String formatDateTime = this.getHoraCierre().format(formatter);
		return formatDateTime;
	}
	
	public static Predicate<VelaBitmex> fechaIgualOSuperiorA(LocalDateTime fecha){
		return v -> v.getHoraCierre().isEqual(fecha) || v.getHoraCierre().isAfter(fecha);
	}
	
	public static Comparator<VelaBitmex> ORDEN_ASCENDENTE = (v1,v2) -> v1.getHoraCierre().compareTo(v2.getHoraCierre());

}