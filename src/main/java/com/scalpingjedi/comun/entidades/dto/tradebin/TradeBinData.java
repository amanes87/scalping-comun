package com.scalpingjedi.comun.entidades.dto.tradebin;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TradeBinData {
	@SerializedName("timestamp")
	private String timestamp = null;

	@SerializedName("symbol")
	private String symbol = null;

	@SerializedName("open")
	private Double open = null;

	@SerializedName("high")
	private Double high = null;

	@SerializedName("low")
	private Double low = null;

	@SerializedName("close")
	private Double close = null;

	@SerializedName("trades")
	private BigDecimal trades = null;

	@SerializedName("volume")
	private BigDecimal volume = null;

	@SerializedName("vwap")
	private Double vwap = null;

	@SerializedName("lastSize")
	private BigDecimal lastSize = null;

	@SerializedName("turnover")
	private BigDecimal turnover = null;

	@SerializedName("homeNotional")
	private Double homeNotional = null;

	@SerializedName("foreignNotional")
	private Double foreignNotional = null;
}
