package com.scalpingjedi.comun.entidades.dto.liquidation;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LiquidationDTO {


	private String table;

	String action;

	LiquidationData[] data;
}
