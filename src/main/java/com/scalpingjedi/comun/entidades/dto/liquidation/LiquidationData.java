package com.scalpingjedi.comun.entidades.dto.liquidation;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class LiquidationData {
	
	@SerializedName("orderID")
	  private String orderID = null;

	  @SerializedName("symbol")
	  private String symbol = null;

	  @SerializedName("side")
	  private String side = null;

	  @SerializedName("price")
	  private Double price = null;

	  @SerializedName("leavesQty")
	  private Long leavesQty = null;

	
}
