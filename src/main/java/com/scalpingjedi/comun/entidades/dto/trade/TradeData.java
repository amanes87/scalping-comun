package com.scalpingjedi.comun.entidades.dto.trade;

import java.math.BigDecimal;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TradeData {

	 @SerializedName("timestamp")
	  private String timestamp = null;

	  @SerializedName("symbol")
	  private String symbol = null;

	  @SerializedName("side")
	  private String side = null;

	  @SerializedName("size")
	  private BigDecimal size = null;

	  @SerializedName("price")
	  private Double price = null;

	  @SerializedName("tickDirection")
	  private String tickDirection = null;

	  @SerializedName("trdMatchID")
	  private String trdMatchID = null;

	  @SerializedName("grossValue")
	  private BigDecimal grossValue = null;

	  @SerializedName("homeNotional")
	  private Double homeNotional = null;

	  @SerializedName("foreignNotional")
	  private Double foreignNotional = null;
	
}
