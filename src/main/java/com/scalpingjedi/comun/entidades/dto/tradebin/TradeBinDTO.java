package com.scalpingjedi.comun.entidades.dto.tradebin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TradeBinDTO {

	String table;
	
	String action;
	
	TradeBinData [] data;
	
}
