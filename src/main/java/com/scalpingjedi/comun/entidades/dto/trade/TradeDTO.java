package com.scalpingjedi.comun.entidades.dto.trade;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TradeDTO {

	private String table;

	String action;

	TradeData[] data;

}
