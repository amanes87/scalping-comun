package com.scalpingjedi.comun.entidades;



import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Liquidacion {

	private Boolean shortLiquidation;
	private Boolean longLiquidation;
	private Long shortLiquidationQuantity;
	private Long longLiquidationQuantity;
	private BigDecimal precioMedioShortLiquidation;
	private BigDecimal precioMedioLongLiquidation;
	
	
	public Liquidacion() {
		this.shortLiquidation = false;
		this.longLiquidation = false;
		this.shortLiquidationQuantity = 0L;
		this.longLiquidationQuantity = 0L;
		this.precioMedioShortLiquidation = BigDecimal.ZERO;
		this.precioMedioLongLiquidation = BigDecimal.ZERO;
	}
}
