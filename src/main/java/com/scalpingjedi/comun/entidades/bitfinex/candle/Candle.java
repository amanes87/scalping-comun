package com.scalpingjedi.comun.entidades.bitfinex.candle;

import java.time.Instant;
import java.time.ZoneOffset;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "CANDLE")
public class Candle {

	@Id
	private Instant timestamp;
	
	private Double open;
	
	private Double high;
	
	private Double low;
	
	private Double close;
	
	private Double volume;
	
	public Candle(Instant timestamp, Double open, Double close, Double high, Double low, Double volume) {
		this.timestamp = timestamp;
		this.open = open;
		this.close = close;
		this.high = high;
		this.low = low;
		this.volume = volume;
	}

	public Candle(String timestamp, String open, String close, String high, String low, String volume) {
		this.timestamp = Instant.ofEpochMilli(Long.valueOf(timestamp));
		this.open = Double.valueOf(open);
		this.close = Double.valueOf(close);
		this.high = Double.valueOf(high);
		this.low = Double.valueOf(low);
		this.volume = Double.valueOf(volume);
	}

	@Override
	public String toString() {
		return "Candle [timestamp=" + timestamp + ", open=" + open + ", high=" + high + ", low=" + low + ", close="
				+ close + ", volume=" + volume + "]";
	}
	
	
}
