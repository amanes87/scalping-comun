package com.scalpingjedi.comun.entidades;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Orden {

	private Long shortVolume;
	private Long longVolume;
	
	private Double shortPrice;
	private Double longPrice;
	
	
	public Orden() {
		this.shortVolume = 0L;
		this.longVolume = 0L;
	}
}
