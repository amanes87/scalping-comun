package com.scalpingjedi.comun.entidades.temporalidades;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.scalpingjedi.comun.entidades.Liquidacion;
import com.scalpingjedi.comun.entidades.VelaBitmex;
import com.scalpingjedi.comun.entidades.dto.trade.TradeData;
import com.scalpingjedi.comun.entidades.dto.tradebin.TradeBinData;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "VELA_005")
public class Vela_005 extends VelaBitmex {

	public Vela_005(TradeBinData tradeBinData) {
		super(tradeBinData);
	}
	
	public Vela_005(TradeBinData tradeBinData,Liquidacion liquidacion) {
		super(tradeBinData,liquidacion);
	}

	public Vela_005(VelaBitmex vela) {
		super(vela);
	}

	public static Vela_005 generarVelaSiguiente(Vela_005 velaAnterior) {
		Vela_005 nuevaVela = new Vela_005();
		nuevaVela.setHoraCierre(velaAnterior.getHoraCierre().plusMinutes(5L));
		nuevaVela.setOpen(velaAnterior.getClose());
		nuevaVela.setHigh(velaAnterior.getClose());
		nuevaVela.setLow(velaAnterior.getClose());
		nuevaVela.setClose(velaAnterior.getClose());
		nuevaVela.setVolumen(BigDecimal.ZERO);
		nuevaVela.setVolumenBuy(BigDecimal.ZERO);
		nuevaVela.setVolumenSell(BigDecimal.ZERO);
		nuevaVela.setCantidadBuy(0);
		nuevaVela.setCantidadSell(0);
		nuevaVela.setDineroAcumuladoBuy(BigDecimal.ZERO);
		nuevaVela.setDineroAcumuladoSell(BigDecimal.ZERO);
		return nuevaVela;
	}

	public synchronized void acumularTrade(TradeData trade) {
		if (trade.getSide().equals("Buy")) {
			setVolumenBuy(getVolumenBuy().add(trade.getSize()));
			setCantidadBuy(getCantidadBuy() + 1);
			setDineroAcumuladoBuy(getDineroAcumuladoBuy().add(trade.getSize().multiply(new BigDecimal(trade.getPrice()))));
		}else {
			setVolumenSell(getVolumenSell().add(trade.getSize()));
			setCantidadSell(getCantidadSell() +1);
			setDineroAcumuladoSell(getDineroAcumuladoSell().add(trade.getSize().multiply(new BigDecimal(trade.getPrice()))));
		}
		setVolumen(getVolumen().add(trade.getSize()));
		if (trade.getPrice() > getHigh().doubleValue()) {
			setHigh(new BigDecimal(trade.getPrice()));
		}
		if (trade.getPrice() < getLow().doubleValue()) {
			setLow(new BigDecimal(trade.getPrice()));
		}
		setClose(new BigDecimal(trade.getPrice()));
		
	}

}
