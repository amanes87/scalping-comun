package com.scalpingjedi.comun.entidades;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DataBitmex {
	
	private String timestamp;

	private String symbol;

	private String side;

	private Double price;

	private String tickDirection;

	private String trdMatchID;

	private Double grossValue;

	private Double homeNotional;
	
	private Double foreignNotional;

	@Override
	public String toString() {
		return "Data [timestamp=" + timestamp + ", symbol=" + symbol + ", side=" + side + ", price=" + price
				+ ", tickDirection=" + tickDirection + ", trdMatchID=" + trdMatchID + ", grossValue=" + grossValue
				+ ", homeNotional=" + homeNotional + ", foreignNotional=" + foreignNotional + "]";
	}

}
