package com.scalpingjedi.comun.servicios.trades;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.trades.ResumenBigOrder;

public interface ResumenBigOrderRepository extends CrudRepository<ResumenBigOrder, Long>{

	@Query(nativeQuery = true, value = "SELECT * FROM resumen_big_orders order by fecha asc")
	List<ResumenBigOrder> todosRegistrosAscendiente();
	
	@Query(value = "SELECT t FROM ResumenBigOrder t WHERE t.fecha >= ?1 ")
	List<ResumenBigOrder> findAllAfterDate(LocalDateTime timePeriodStart);

}
