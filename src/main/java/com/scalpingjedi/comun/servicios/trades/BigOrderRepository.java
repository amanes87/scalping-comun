package com.scalpingjedi.comun.servicios.trades;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.trades.BigOrder;
import com.scalpingjedi.comun.entidades.trades.ResumenBigOrder;

public interface BigOrderRepository extends CrudRepository<BigOrder, Long>{

	@Query(nativeQuery = true, value = "SELECT * FROM big_orders order by hora asc")
	List<BigOrder> todosRegistrosAscendiente();
	
	@Query(value = "SELECT t FROM BigOrder t WHERE t.hora >= ?1 order by volumen desc, hora desc")
	List<BigOrder> findAllAfterDate(LocalDateTime timePeriodStart);

}
