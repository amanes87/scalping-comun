package com.scalpingjedi.comun.servicios.trades;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.trades.ResumenTrade;

public interface ResumenTradeRepository extends CrudRepository<ResumenTrade, Long>{

	@Query(nativeQuery = true, value = "SELECT * FROM resumen_trade order by fecha_cierre asc")
	public List<ResumenTrade> todosRegistrosAscendiente();
}
