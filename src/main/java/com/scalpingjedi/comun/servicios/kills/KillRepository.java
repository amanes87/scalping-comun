package com.scalpingjedi.comun.servicios.kills;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.kills.Kill;

public interface KillRepository extends CrudRepository<Kill, Long>{

	@Query(nativeQuery = true, value = "SELECT * FROM kill order by fecha asc")
	List<Kill> todosRegistrosAscendiente();
	
	
	@Query(nativeQuery = true, value = "SELECT * FROM kill k where k.fecha = (SELECT max(k2.fecha) from kill k2)")
	List<Kill> getLastKill();

}
