package com.scalpingjedi.comun.servicios.bitfinex.candle;

import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.bitfinex.candle.Candle;

public interface CandleRepository extends CrudRepository<Candle, Long>{

}
