package com.scalpingjedi.comun.servicios.pools;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.pools.Pool;

public interface PoolRepository extends CrudRepository<Pool, Long>{


	@Query(nativeQuery = true, value = "SELECT * FROM Pools order by hora asc")
	public List<Pool> todosRegistrosAscendiente();
	
//	@Query(nativeQuery = true, value = "SELECT distinct p.hora, p.precio,"
//			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '100x') as a100,"
//			+ " (select coalesce(sum(p2.volumen),0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '50x') as a50, "
//			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '25x') as a25, "
//			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio ) as total "
//			+ " from pools p where p.hora >= ?1")
//	public List<Object[]> obtenerPoolsAgrupados(LocalDateTime hora);
	
	@Query(nativeQuery = true, value = "SELECT distinct p.hora, p.precio,"
			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '100x') as a100,"
			+ " (select coalesce(sum(p2.volumen),0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '50x') as a50, "
			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '25x') as a25, "
			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '10x') as a10, "
			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio and p2.apalancamiento = '5x') as a5, "
			+ " (select COALESCE(sum(p2.volumen), 0) from pools p2 where p2.hora=p.hora and p2.precio = p.precio ) as total "
			+ " from pools p where p.hora = ?1 order by p.precio")
	public List<Object[]> obtenerPoolsAgrupadosPorHora(LocalDateTime hora);
	
	
	@Query(nativeQuery = true, value = "SELECT min(precio) from pools where hora >= ?1 ")
	public BigDecimal obtenerMinPrecio(LocalDateTime hora);
	
	@Query(nativeQuery = true, value = "SELECT max(precio) from pools where hora >= ?1")
	public BigDecimal obtenerMaxPrecio(LocalDateTime hora);
	
	
	@Query(nativeQuery = true, value = "SELECT * FROM pools where hora = (select max(p1.hora) from pools p1) ")
	public List<Pool> obtenerUltimosPools();
	
	
	
			
			
	
}
