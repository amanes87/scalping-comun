package com.scalpingjedi.comun.servicios;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.scalpingjedi.comun.entidades.temporalidades.Vela_005;

public interface Vela_005Repository extends CrudRepository<Vela_005, Long>{

	@Query(value = "SELECT v FROM Vela_005 v WHERE v.horaCierre >= ?1 AND v.horaCierre <= ?2")
	List<Vela_005> findAllBetweenDates(LocalDateTime timePeriodStartInicio, LocalDateTime timePeriodStartFinal);

	@Query(value = "SELECT v FROM Vela_005 v WHERE v.horaCierre >= ?1 ")
	List<Vela_005> findAllAfterDate(LocalDateTime timePeriodStart);
	
	@Query(value = "SELECT v FROM Vela_005 v WHERE v.horaCierre = ?1 ")
	Vela_005 findVelaDate(LocalDateTime hora);
	
	@Query(nativeQuery = true, value = "SELECT * FROM Vela_005 order by hora_cierre asc")
	public List<Vela_005> todosRegistrosAscendiente();
}
